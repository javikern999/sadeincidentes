const express = require('express');// para reqyerur el modulo express
const router = express.Router(); // encargado de crear las rutas


//RUTAS------------------------------>
router.get('/', (req, res) => {
  res.render('index', { title: 'Incidentes y Despliegues' });
});

router.get('/deploy', (req, res) => {
  res.render('deploy', { title: 'Despliegues' });
});

/*router.get('/', (req, res) => {
  res.render('index.html', { title: 'index' });
}); */

router.get('/priorizados', (req, res) => {
  res.render('priorizados', { title: 'Priorizados'})
});



router.get('/contact', (req, res) => {
  res.render('contact', { title: 'Contact Page' });
});

module.exports = router;

//RUTAS!!!!!!
