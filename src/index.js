const express = require('express'); //llamar las dependencias
const morgan = require('morgan');
const path = require('path');
const app = express(); //constante para levantar un servidor

// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views')); //la carpeta view esta en esta direccion
app.set('view engine', 'ejs');
/*app.engine('html', requiere('ejs').renderFile);  [Esto es para ejs lea archivos html] */

// middlewares
app.use(morgan('dev'));

// routes del servidor
app.use(require('./routes')); //requiero desde la carpeta routes el index.js

// Archivos estaticos
app.use(express.static(path.join(__dirname, 'public'))); //publucar la carpeta 'public' para que todos puedan acceder a el

// escuchando el  Server
app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
});
